package game;


import game.contract.IBlock;
import game.contract.IBonus;
import game.contract.ISprite;
import game.contract.ISpriteGame;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;

import javax.swing.ImageIcon;

public class Block implements ISprite,IBlock{
	
	private IBonus bonus;
	private int x;
	private int y;
	private Image image;
	private int lives;
	private ISpriteGame mainGame;
	
	private boolean visible;
	public boolean getVisible(){
		return this.visible;
	}
	public void setVisible(boolean visible){
		this.visible=visible;
		if (bonus!=null)
			bonus.Start(this.x,this.y);
	}
	
	public Block(int blockType,int x, int y,Bonus bonus,ISpriteGame mainGame) {
		this.mainGame=mainGame;
		visible = true;
		this.bonus = bonus;
		this.x=x;
		this.y=y;
		ImageIcon ii;
		switch (blockType){
		case 1:
			ii = new ImageIcon(this.getClass().getResource("images/block-blue.png"));
			this.lives=1;
			break;
		case 2:
			ii = new ImageIcon(this.getClass().getResource("images/block-yellow.png"));
			this.lives=2;
			break;
		case 3:
			ii = new ImageIcon(this.getClass().getResource("images/block-green.png"));
			this.lives=3;
			break;
		case 4:
			ii = new ImageIcon(this.getClass().getResource("images/block-red.png"));
			this.lives=4;
			break;
		case 5:
			ii = new ImageIcon(this.getClass().getResource("images/block-cracked.png"));
			this.lives=5;
			break;
		case 6:
			ii= new ImageIcon(this.getClass().getResource("images/block-steel.png"));
			this.lives=6;
			break;
		default:
			ii = new ImageIcon(this.getClass().getResource("images/block-shade.png"));
			this.lives=0;
			break;
		}
		
		this.image = ii.getImage();
	}

	public void Update() {
		
	}
	
	public void Draw(GamePanel gamePanel,Graphics2D g2d){
		g2d.drawImage(image,x,y,gamePanel);
	}
	
	public Rectangle getBounds() {
        return new Rectangle(x, y, this.image.getWidth(null), this.image.getHeight(null));
    }
	
	public void hitBlock(){
		MainGame.playSound("block.wav");
		this.lives--;
		switch (this.lives){
		case 1:
			//MainGame.playSound("block.wav");
			this.image=new ImageIcon(this.getClass().getResource("images/block-blue.png")).getImage();
			break;
		case 2:
			//MainGame.playSound("block.wav");
			this.image=new ImageIcon(this.getClass().getResource("images/block-yellow.png")).getImage();
			break;
		case 3:
			//MainGame.playSound("block.wav");
			this.image=new ImageIcon(this.getClass().getResource("images/block-green.png")).getImage();
			break;
		case 4:
			//MainGame.playSound("block.wav");
			this.image=new ImageIcon(this.getClass().getResource("images/block-red.png")).getImage();
			break;
		case 5:
			//MainGame.playSound("block.wav");
			this.image=new ImageIcon(this.getClass().getResource("images/block-cracked.png")).getImage();
			break;
		default:
			this.setVisible(false);
			mainGame.updateBlockNumber(-1);
		}

	}
	
	public void clickBlock(){
		if (this.lives==6)
			this.lives=0;
		else
			this.lives++;
		switch (this.lives){
		case 0:
			this.image=new ImageIcon(this.getClass().getResource("images/block-shade.png")).getImage();
			break;
		case 1:
			this.image=new ImageIcon(this.getClass().getResource("images/block-blue.png")).getImage();
			break;
		case 2:
			this.image=new ImageIcon(this.getClass().getResource("images/block-yellow.png")).getImage();
			break;
		case 3:
			this.image=new ImageIcon(this.getClass().getResource("images/block-green.png")).getImage();
			break;
		case 4:
			this.image=new ImageIcon(this.getClass().getResource("images/block-red.png")).getImage();
			break;
		case 5:
			this.image=new ImageIcon(this.getClass().getResource("images/block-cracked.png")).getImage();
			break;
		case 6:
			this.image=new ImageIcon(this.getClass().getResource("images/block-steel.png")).getImage();
			break;			

		}
	}
}
