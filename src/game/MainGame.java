package game;

import game.contract.IBonusInfo;
import game.contract.IMouseSprite;
import game.contract.ISprite;
import game.contract.ISpriteGame;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.List;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.JOptionPane;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import arkanoid.sharedcomponent.Score;
import sharedContracts.IPlayerAchievements;
import stepmania.sharedComponent.ScoreTable;
import milionerzy.sharedComponent.Login;


public class MainGame implements ISpriteGame, IBonusInfo {

	private LevelStructure levelStructure;
	private ScoreTable scoreTable;
	private int lives;
	private int level;
	private int blockNumber;
	private int rowNumber;
	private int columnNumber;
	private int bonusInfoTime;
	private String bonusInfoText = "";
	private Font font1 = new Font("Pristina", Font.BOLD, 36);
    private Font font2 = new Font("Pristina", Font.BOLD, 42);
	private int messageCounter;
	private String message;
	private List<Block> blockList;
	private List<Bonus> bonusList;
	private List<ISprite> spriteList;
	

	private enum GameState {
		Menu, Selection, Game, HiScore, LevelEditor, NewLevelMessage, GameOverMessage, WinMessage
	};

	private GameState gameState = GameState.Menu;

	private Score score;

	public int GetScore() {
		return this.score.getScore();
	}

	public void setScore(Score score) {
		this.score = score;
	}

	private Bouncer bouncer;

	public void setBouncer(Bouncer bouncer) {
		this.bouncer = bouncer;
	}

	private GameBackground gameBackground;

	public void setGameBackground(GameBackground gameBackground) {
		this.gameBackground = gameBackground;
	}

	private Ball ball;

	public void setBall(Ball ball) {
		this.ball = ball;
	}

	private LaserBeam laserBeam;

	public void setLaserBeam(LaserBeam laserBeam) {
		this.laserBeam = laserBeam;
	}

	private Menu menu;

	public void setMenu(Menu menu) {
		this.menu = menu;
	}

	private HighScores highScores;

	public void setHighScores(HighScores highScores) {
		this.highScores = highScores;
	}
	
	private IMouseSprite levelEditorBackground;

	public void setLevelEditorBackground(
			LevelEditorBackground levelEditorBackground) {
		this.levelEditorBackground = levelEditorBackground;
	}

	private Login login;

	public void setLogin(Login login) {
		this.login = login;
	}

	public MainGame(ScoreTable scoreTable, Login login) {
		this.rowNumber = 6;
		this.columnNumber = 14;
		this.scoreTable = scoreTable;
		this.login = login;
		message = "";
		blockList = new ArrayList<Block>();
		bonusList = new ArrayList<Bonus>();
		spriteList = new ArrayList<ISprite>();
		boolean first = true;
		try {
			String errorMessage = "";
			do {
				if (!first) {
					if (login.getLogin().length() < 3) {
						errorMessage = "//must be longer than 3";
					}
					if (login.getLogin().length() > 8) {
						errorMessage = "//must be shorter than 8";
					}
				}
				login.setLogin("");
				login.setLogin(JOptionPane.showInputDialog(null,
						"Enter your name : " + errorMessage, "Name Entry", 1));
				first = false;
			} while (login.getLogin().length() < 3
					|| login.getLogin().length() > 8);
		} catch (Exception e) {
			login.setLogin("NoName");
		}
		scoreTable.loadScoreTable("ScoreTable.txt");
		if (scoreTable.getCount() < 6) {
			scoreTable.clearScoreTable();

			scoreTable.addScore(new Login("Piotr"), new Score(90));
			scoreTable.addScore(new Login("Marcin"), new Score(80));
			scoreTable.addScore(new Login("Adam"), new Score(70));
			scoreTable.addScore(new Login("Kuba"), new Score(60));
			scoreTable.addScore(new Login("Ania"), new Score(50));
			scoreTable.addScore(new Login("Jarek"), new Score(40));
			scoreTable.saveScoreTable();
		}
		MainGame.playMusic("gameMusic.wav");

	}

	public void Update(int keyPressed, int keyReleased, int mousePosX,
			int mousePosY, boolean mouseClicked) {
		switch (gameState) {
		case Game:
			for (ISprite object : spriteList) {
				if (object.getVisible()) {
					object.Update();
				}
			}
			bouncer.Update(keyPressed, keyReleased);
			break;
		case Menu:
			menu.Update(keyPressed, keyReleased);
			break;
		case HiScore:
			highScores.Update(keyPressed, keyReleased);
			break;
		case LevelEditor:
			levelEditorBackground.Update(mousePosX, mousePosY, mouseClicked);
			break;
		case NewLevelMessage:
			messageCounter--;
			if (messageCounter <= 0) {
				messageCounter = 0;
				if (level == 0)
					InitCustomGame();
				else
					startNewLevel();
			}
			break;
		case GameOverMessage:
			messageCounter--;
			if (messageCounter <= 0) {
				messageCounter = 0;
				if (this.score.getScore() > scoreTable.getScoreTable().get(5).getScore().getScore()){
					MainGame.playSound("applause.wav");
				}
				startHiScore();
			}
			break;
		case WinMessage:
			messageCounter--;
			if (messageCounter <= 0) {
				messageCounter = 0;
				startMenu();
			}
			break;

		}
	}

	public void Draw(GamePanel gamePanel, Graphics2D g2d) {
		
		g2d.setFont(font2);
		switch (gameState) {
		case Game:
			for (ISprite object : spriteList) {
				if (object.getVisible())
					object.Draw(gamePanel, g2d);
			}
			bouncer.Draw(gamePanel, g2d);
			g2d.setColor(new Color(65, 146, 187));
			
			g2d.drawString("Lives: " + lives, 30, 60);
			g2d.drawString("Score: " + score, 600, 60);
			g2d.drawString("Lives: " + lives, 31, 61);
			g2d.drawString("Score: " + score, 601, 61);
			g2d.drawString("Lives: " + lives, 32, 62);
			g2d.drawString("Score: " + score, 602, 62);
			g2d.setColor(new Color(109, 209, 235));
			g2d.drawString("Lives: " + lives, 33, 63);
			g2d.drawString("Score: " + score, 603, 63);
			if (bonusInfoTime > 0) {
				g2d.setColor(Color.RED);
				g2d.drawString(bonusInfoText, 300, 300);
				bonusInfoTime--;
			}
			break;
		case Menu:
			menu.Draw(gamePanel, g2d);
			g2d.setColor(new Color(65, 146, 187));
			for (int i = 1; i < 5; i++) {
				g2d.drawString("Hi " + login.getLogin() + "!", 420 + i, 200 + i);
			}
			g2d.setColor(new Color(245, 81, 108));
			g2d.drawString("Hi " + login.getLogin() + "!", 425, 200);
			break;
		case HiScore:
			highScores.Draw(gamePanel, g2d);
			font1 = new Font("", Font.BOLD, 63);
			g2d.setColor(Color.WHITE);
			List<IPlayerAchievements> scoreList = scoreTable.getScoreTable();
			for (int i = 0; i < 6; i++) {
				if (i == 5
						&& scoreList.get(i).getLogin().getLogin() == login
								.getLogin()
						&& scoreList.get(i).getScore().getScore() == score
								.getScore()) {
					g2d.setColor(Color.RED);
				} else if (i < 8) {
					if (scoreList.get(i).getLogin().getLogin() == login
							.getLogin()
							&& scoreList.get(i).getScore().getScore() == score
									.getScore()
							&& scoreList.get(i).getScore().getScore() != scoreList
									.get(i + 1).getScore().getScore()) {
						g2d.setColor(Color.RED);
					}
				}
				g2d.drawString(scoreList.get(i).getLogin().getLogin(), 190,
						310 + i * 82);
				g2d.drawString("" + scoreList.get(i).getScore().getScore(),
						500, 310 + i * 82);
				g2d.setColor(Color.WHITE);
			}
			break;
		case LevelEditor:
			levelEditorBackground.Draw(gamePanel, g2d);
			for (Block block : blockList) {
				block.Draw(gamePanel, g2d);
			}
			break;
		case NewLevelMessage:
			spriteList.get(0).Draw(gamePanel, g2d);
			g2d.setColor(Color.DARK_GRAY);
			font1 = new Font("PRISTINA", Font.BOLD, 80);
			g2d.setFont(font1);
			g2d.drawString(message, 290, 400);
			font1 = new Font("PRISTINA", Font.BOLD, 36);
			g2d.setFont(font1);
			break;
		case GameOverMessage:
			spriteList.get(0).Draw(gamePanel, g2d);
			g2d.setColor(Color.DARK_GRAY);
			font1 = new Font("PRISTINA", Font.BOLD, 80);
			g2d.setFont(font1);
			g2d.drawString(message, 230, 400);
			font1 = new Font("PRISTINA", Font.BOLD, 36);
			g2d.setFont(font1);
			break;
		case WinMessage:
			spriteList.get(0).Draw(gamePanel, g2d);
			g2d.setColor(Color.DARK_GRAY);
			font1 = new Font("PRISTINA", Font.BOLD, 80);
			g2d.setFont(font1);
			g2d.drawString(message, 150, 400);
			font1 = new Font("PRINTINA", Font.BOLD, 36);
			g2d.setFont(font1);
			break;
		}

	}

	public void updateBlockNumber(int blockNumber) {
		this.blockNumber += blockNumber;
		this.score.addToScore(10);
		if (this.blockNumber <= 0) {
			MainGame.playSound("levelWin.wav");
			if (level > 0) {
				level++;
				this.message = "Level " + level;
				this.gameState = GameState.NewLevelMessage;
				this.messageCounter = 400;
			} else {
				this.message = "Congratulations!";
				this.gameState = GameState.WinMessage;
				this.messageCounter = 700;
			}
		}
	}

	public void setBonusInfoText(String bonusInfoText) {
		this.bonusInfoText = bonusInfoText;
		this.bonusInfoTime = 100;
	}

	public void updateLives(int update) {
		this.lives += update;
		if (this.lives == 0) {
			MainGame.playSound("gameOver.wav");
			this.message = "Game Over";
			this.gameState = GameState.GameOverMessage;
			this.messageCounter = 700;
		}
		if (update<0)
		{
			bouncer.setLaserActive(false);
		}
	}

	public void updateScore(int update) {
		this.score.addToScore(update);

	}

	public void startMenu() {
		this.score = new Score(0);
		gameState = GameState.Menu;
	}

	public void startHiScore() {
		if (score.getScore() > scoreTable.getMaxScore().getScore()) {
			scoreTable.addScore(login, score);
			scoreTable.removeScore(scoreTable.getScoreTable().size() - 1);
			scoreTable.saveScoreTable();
		}
		gameState = GameState.HiScore;
	}

	public void startCustomGame() {
		this.lives = 3;
		this.level = 0;
		this.score.clearScore();
		this.message = "Custom";
		this.messageCounter = 400;
		spriteList = new ArrayList<ISprite>();
		spriteList.add(gameBackground);
		this.gameState = GameState.NewLevelMessage;
	}

	public void startGame() {
		this.lives = 3;
		this.level = 1;
		this.score.clearScore();
		this.message = "Level " + level;
		this.messageCounter = 400;
		spriteList = new ArrayList<ISprite>();
		spriteList.add(gameBackground);
		bouncer.setImage("images/bouncer.png");
		bouncer.setLaserActive(false);
		this.gameState = GameState.NewLevelMessage;
	}

	public void startNewLevel() {
		blockList = new ArrayList<Block>();
		bonusList = new ArrayList<Bonus>();
		spriteList = new ArrayList<ISprite>();
		ApplicationContext context;
		switch (level % 10) {
		case 1:
			context = new ClassPathXmlApplicationContext("level-1.xml");
			levelStructure = (LevelStructure) context.getBean("level-1");
			break;
		case 2:
			context = new ClassPathXmlApplicationContext("level-2.xml");
			levelStructure = (LevelStructure) context.getBean("level-2");
			break;
		case 3:
			context = new ClassPathXmlApplicationContext("level-3.xml");
			levelStructure = (LevelStructure) context.getBean("level-3");
			break;
		case 4:
			context = new ClassPathXmlApplicationContext("level-4.xml");
			levelStructure = (LevelStructure) context.getBean("level-4");
			break;
		case 5:
			context = new ClassPathXmlApplicationContext("level-5.xml");
			levelStructure = (LevelStructure) context.getBean("level-5");
			break;
		case 6:
			context = new ClassPathXmlApplicationContext("level-6.xml");
			levelStructure = (LevelStructure) context.getBean("level-6");
			break;
		case 7:
			context = new ClassPathXmlApplicationContext("level-7.xml");
			levelStructure = (LevelStructure) context.getBean("level-7");
			break;
		case 8:
			context = new ClassPathXmlApplicationContext("level-8.xml");
			levelStructure = (LevelStructure) context.getBean("level-8");
			break;
		case 9:
			context = new ClassPathXmlApplicationContext("level-9.xml");
			levelStructure = (LevelStructure) context.getBean("level-9");
			break;
		default:
			context = new ClassPathXmlApplicationContext("level-1.xml");
			levelStructure = (LevelStructure) context.getBean("level-1");
			break;
		}
		int[] blockType = new int[84];
		blockType = levelStructure.getBlockType();
		bouncer.NewLevel(330, 700);
		ball.startNewLevel(320, 650);
		spriteList.add(gameBackground);
		spriteList.add(ball);
		int helper = 0;
		blockNumber = 0;
		for (int j = 0; j < rowNumber; j++) {
			for (int i = 0; i < columnNumber; i++) {
				if (!(blockType[helper] == 0)) {
					Bonus bo;
					if ((int) (Math.random() * 8) == 1)
						bo = new Bonus(bouncer, this, ball);
					else
						bo = null;
					Block bl = new Block(blockType[helper], (j % 2) * 25 + i
							* 50 + 45, j * 50 + 120, bo, this);
					blockList.add(bl);
					spriteList.add(bl);
					if (bo != null) {
						bonusList.add(bo);
						spriteList.add(bo);
					}
					blockNumber++;
				}
				helper++;
			}
		}

		laserBeam.setBlockList(blockList);
		spriteList.add(laserBeam);
		ball.setBlockList(blockList);
		gameState = GameState.Game;
	}

	public void InitCustomGame() {
		blockList = new ArrayList<Block>();
		bonusList = new ArrayList<Bonus>();
		int[] customBlockType;
		try {
			ApplicationContext context = new ClassPathXmlApplicationContext(
					"file:" + System.getProperty("user.dir")
							+ "\\custom-level.xml");
			levelStructure = (LevelStructure) context.getBean("custom-level");
			customBlockType = new int[84];
			customBlockType = levelStructure.getBlockType();
		} catch (Exception e) {
			customBlockType = new int[84];
			for (int i = 0; i < 84; i++)
				customBlockType[i] = 1;
		}
		this.level = 0;
		bouncer.NewLevel(330, 700);
		ball.startNewLevel(320, 650);
		spriteList.add(ball);
		int helper = 0;
		blockNumber = 0;
		for (int j = 0; j < rowNumber; j++) {
			for (int i = 0; i < columnNumber; i++) {
				if (!(customBlockType[helper] == 0)) {
					Bonus bo;
					if ((int) (Math.random() * 8) == 1)
						bo = new Bonus(bouncer, this, ball);
					else
						bo = null;
					Block bl = new Block(customBlockType[helper], (j % 2) * 25
							+ i * 50 + 45, j * 50 + 120, bo, this);
					blockList.add(bl);
					spriteList.add(bl);
					if (bo != null) {
						bonusList.add(bo);
						spriteList.add(bo);
					}
					blockNumber++;
				}
				helper++;
			}
		}
		laserBeam.setBlockList(blockList);
		spriteList.add(laserBeam);
		ball.setBlockList(blockList);
		bouncer.setImage("images/bouncer.png");
		bouncer.setLaserActive(false);
		gameState = GameState.Game;
	}

	public void startLevelEditor() {
		System.out.println(System.getProperty("user.dir") + "\\custom-level");
		blockList = new ArrayList<Block>();
		int[] customBlockType;
		try {
			ApplicationContext context = new ClassPathXmlApplicationContext(
					"file:" + System.getProperty("user.dir")
							+ "\\custom-level.xml");
			levelStructure = (LevelStructure) context.getBean("custom-level");
			customBlockType = new int[84];
			customBlockType = levelStructure.getBlockType();
		} catch (Exception e) {
			customBlockType = new int[84];
			for (int i = 0; i < 84; i++)
				customBlockType[i] = 0;
		}
		int helper = 0;
		for (int j = 0; j < rowNumber; j++) {
			for (int i = 0; i < columnNumber; i++) {
				Block bl = new Block(customBlockType[helper], (j % 2) * 25 + i
						* 50 + 45, j * 50 + 120, null, this);
				blockList.add(bl);
				blockNumber++;
				helper++;
			}
		}
		levelEditorBackground.setBlockList(blockList);
		levelEditorBackground.setBlockType(customBlockType);
		levelEditorBackground.setGamePanel(this);
		gameState = GameState.LevelEditor;
	}

	public static synchronized void playSound(final String path) {
	    new Thread(new Runnable() {
	      public void run() {
	        try {
	          Clip clip = AudioSystem.getClip();
	          AudioInputStream inputStream = AudioSystem.getAudioInputStream(MainGame.class.getResourceAsStream("/sounds/" + path));
	          clip.open(inputStream);
	          clip.start(); 
	        } catch (Exception e) {
	        }
	      }
	    }).start();
	  }
	public static synchronized void playMusic(final String path) {
	    new Thread(new Runnable() {
	      public void run() {
	        try {
	          Clip clip = AudioSystem.getClip();
	          AudioInputStream inputStream = AudioSystem.getAudioInputStream(MainGame.class.getResourceAsStream("/sounds/" + path));
	          clip.open(inputStream);
	          clip.loop(-1); 
	        } catch (Exception e) {
	        
	        }
	      }
	    }).start();
	  }
}
