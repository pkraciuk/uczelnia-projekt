package game;

import game.contract.IActiveSprite;
import game.contract.IBouncer;
import game.contract.IShootLaser;
import game.contract.ISprite;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;

import javax.swing.ImageIcon;

public class Bouncer implements IActiveSprite,ISprite,IBouncer{
	
	private int previousKeyPressed;
	private int xSpeed;
	private int ySpeed;
	private int x;
	private int y;

	private Image image;
	public void setImage(String imgPath){
		ImageIcon ii = new ImageIcon(this.getClass().getResource(imgPath));
		this.image = ii.getImage();
	}

	private boolean laserActive;
	public void setLaserActive(boolean laserActive){
		this.laserActive=laserActive;
	}
	
	private boolean visible;
	public boolean getVisible(){
		return this.visible;
	}

	private IShootLaser laserBeam;	
	public void setLaserBeam(IShootLaser laserBeam) {
		this.laserBeam = laserBeam;
	}
	
	public Bouncer(int x, int y) {
		this.visible=true;
		this.x=x;
		this.y=y;
		ImageIcon ii = new ImageIcon(this.getClass().getResource("images/bouncer.png"));
		this.image = ii.getImage();
		this.xSpeed=0;
		this.ySpeed=0;
		this.laserActive=false;
	}
	
	@Override
	public void Update() {
		
	}
	
	public void Update(int keyPressed,int keyReleased){
		if (keyPressed!=previousKeyPressed){
			if(keyPressed == KeyEvent.VK_LEFT){
				this.xSpeed=-5;
			}
			if(keyPressed == KeyEvent.VK_RIGHT){
				this.xSpeed=+5;
			}
			if(keyPressed == KeyEvent.VK_UP){
				this.ySpeed=-2;
			}
			if(keyPressed == KeyEvent.VK_DOWN){
				this.ySpeed=2;
			}
			if(keyPressed==KeyEvent.VK_SPACE){
				if(!laserBeam.getVisible()&&laserActive){
					MainGame.playSound("laser.wav");
					laserBeam.Shoot(this.x+image.getWidth(null)/2,this.y-100);
				}
			}
		}
		if (keyReleased==KeyEvent.VK_LEFT||keyReleased==KeyEvent.VK_RIGHT)
			this.xSpeed=0;
		if (keyReleased==KeyEvent.VK_UP||keyReleased==KeyEvent.VK_DOWN)
			this.ySpeed=0;
		
		if ((xSpeed>0&& x+image.getWidth(null)<770)||(xSpeed<0&&x>30))
			x+=xSpeed;
		if ((ySpeed>0&& y<727)||(ySpeed<0&&y>580))
			y+=ySpeed;
	}
	
	public void Draw(GamePanel gamePanel,Graphics2D g2d){
		g2d.drawImage(image,x,y,gamePanel);
	}

	public Rectangle getBounds() {
        return new Rectangle(x, y, this.image.getWidth(null), this.image.getHeight(null));
    }
	
	public void NewLevel(int x,int y){		
		this.xSpeed=0;
		this.ySpeed=0;
		this.x=x;
		this.y=y;
	}

	
}
