package game;

import game.contract.IBall;
import game.contract.IBlock;
import game.contract.IBouncer;
import game.contract.ISprite;
import game.contract.ISpriteGame;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;
import javax.swing.ImageIcon;


public class Ball implements ISprite,IBall{

	private int x;
	private int y;
	private Image image;
	private IBouncer bouncer;
	private int colisionCounter;
	private int xSpeed;
	private int ySpeed;
	
	private List<IBlock> blockList;
	public void setBlockList(List<Block> blockList) {
		this.blockList= new ArrayList<IBlock>();
		for(Block block:blockList)
		{
			this.blockList.add(block);
		}
	}
	
	private ISpriteGame mainGame;
	public void setMainGame(ISpriteGame gamePanel) {
		this.mainGame = gamePanel;
	}

	private boolean visible;
	public boolean getVisible(){
		return this.visible;
	}

	public Ball(int x, int y,List<IBlock> blockList,IBouncer bouncer) {
		this.visible=true;
		this.x=x;
		this.y=y;
		ImageIcon ii = new ImageIcon(this.getClass().getResource("images/ball.png"));
		this.image = ii.getImage();
		this.visible=true;
		xSpeed=3;
		ySpeed=3;
		this.blockList = blockList;
		this.bouncer =bouncer;
	}
	
	public Rectangle getBounds() {
        return new Rectangle(x, y, this.image.getWidth(null), this.image.getHeight(null));
    }	
	
	public void Update(){
		x+=xSpeed;
		y+=ySpeed;
		if (x<30){
			MainGame.playSound("wall.wav");
			xSpeed=Math.abs(xSpeed);
		}
		if (x>755){
			MainGame.playSound("wall.wav");
			xSpeed=-Math.abs(xSpeed);
		}
		if(y<100){
			MainGame.playSound("wall.wav");
			ySpeed=Math.abs(ySpeed);
		}
		if(y>727)
		{
			MainGame.playSound("lifeLost.wav");
			ySpeed=-Math.abs(ySpeed);
			mainGame.updateLives(-1);
		}
		for (IBlock object :blockList){
			if (object.getVisible() && CheckCollision(object))
			{
				object.hitBlock();
				
				if(CheckCollisionSide(object)==0)
					this.ySpeed*=-1;
				else
					this.xSpeed*=-1;
			}	
		}
		if (CheckCollision(bouncer)&& colisionCounter==0)
		{
			MainGame.playSound("bouncer.wav");
			this.ySpeed=-Math.abs(this.ySpeed);
			this.colisionCounter=1;
		}
		else if(this.colisionCounter>0){
			colisionCounter+=1;
			if (colisionCounter>60)
				colisionCounter=0;
		}
	}
	
	public void Draw(GamePanel gamePanel,Graphics2D g2d){
		g2d.drawImage(image,x,y,gamePanel);
	}
	
	public void setSpeed(int speed){
		if (xSpeed>0)
			this.xSpeed = speed;
		else
			this.xSpeed = -speed;
		if (ySpeed>0)
			this.ySpeed = speed;
		else
			this.ySpeed = -speed;
	}

	private boolean CheckCollision(Object o){
		ISprite bs = (ISprite)o;
		Rectangle r1 = this.getBounds();
		Rectangle r2 = bs.getBounds();
		if (r1.intersects(r2))
			return true;
		return false;
	}
	
	private int CheckCollisionSide(Object o){
		ISprite bs = (ISprite)o;
		Rectangle r1 = this.getBounds();
		Rectangle r2 = bs.getBounds();
		double r1MiddleX=(r1.x+r1.width/2);
	    double r1MiddleY=(r1.y+r1.height/2);
		double r2MiddleX=(r2.x+r2.width/2);
		double r2MiddleY=(r2.y+r2.height/2);
		if ((Math.abs(r1MiddleX-r2MiddleX)<(Math.abs(r1MiddleY-r2MiddleY))))
		{
			//poziomo
			return 0;
		}
		else
		{
			//pionowo
			return 1;
		}
	}
	
	public void startNewLevel(int x, int y){
		colisionCounter=0;
		this.xSpeed=3;
		this.ySpeed=3;
		this.x=x;
		this.y=y;
	}

}
