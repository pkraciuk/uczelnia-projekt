package game;


import game.contract.IActiveSprite;
import game.contract.ISpriteGame;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;

import javax.swing.ImageIcon;


public class Menu implements IActiveSprite {
	
	private String[] imgPaths;
	private int imgNumber;
	private int previousKeyPressed;
	private ISpriteGame mainGame;
	private int x;
	private int y;
	private Image image;
	
	private boolean visible;	
	public boolean GetVisible(){
		return this.visible;
	}

	public Menu(ISpriteGame mainGame, int x, int y) {
		this.imgNumber=0;
		this.imgPaths= new String[] { "images/menu-game.png",
				"images/menu-editor.png", "images/menu-highscore.png", "images/menu-standard.png",
				"images/menu-custom.png", };
		this.mainGame = mainGame;
		this.visible=true;
		this.x=x;
		this.y=y;
		ImageIcon ii = new ImageIcon(this.getClass().getResource(imgPaths[0]));
		this.image = ii.getImage();
	}

	public void Update(int keyPressed,int keyReleased){
		
		if (keyPressed!=previousKeyPressed)
		{
			if(keyPressed == KeyEvent.VK_UP){
				MainGame.playSound("optionChange.wav");
				if (imgNumber==0||imgNumber==1)
					imgNumber+=1;
				else
					imgNumber=0;
				ImageIcon ii = new ImageIcon(this.getClass().getResource(imgPaths[imgNumber]));
				this.image = ii.getImage();
			}
			if(keyPressed == KeyEvent.VK_DOWN){
				MainGame.playSound("optionChange.wav");
				if (imgNumber==1||imgNumber==2)
					imgNumber-=1;
				else
					imgNumber=2;
				ImageIcon ii = new ImageIcon(this.getClass().getResource(imgPaths[imgNumber]));
				this.image = ii.getImage();
			}
			if(keyPressed==KeyEvent.VK_RIGHT){
				if(imgNumber==3){
					MainGame.playSound("optionChange.wav");
					imgNumber=4;
					ImageIcon ii = new ImageIcon(this.getClass().getResource(imgPaths[imgNumber]));
					this.image = ii.getImage();
				}
			}
			if(keyPressed==KeyEvent.VK_LEFT){
				if(imgNumber==4){
					MainGame.playSound("optionChange.wav");
					imgNumber=3;
					ImageIcon ii = new ImageIcon(this.getClass().getResource(imgPaths[imgNumber]));
					this.image = ii.getImage();
				}
			}
			if(keyPressed == KeyEvent.VK_SPACE||keyPressed==KeyEvent.VK_ENTER)
			{
				if (imgNumber==0)
				{
					MainGame.playSound("optionChange.wav");
					imgNumber=3;
					ImageIcon ii = new ImageIcon(this.getClass().getResource(imgPaths[imgNumber]));
					this.image = ii.getImage();
				}
				else if (imgNumber==1)
				{
					MainGame.playSound("accept.wav");
					this.mainGame.startLevelEditor();
				}
				else if (imgNumber==2)
				{
					MainGame.playSound("accept.wav");
					this.mainGame.startHiScore();
				}
				else if (imgNumber==3){
					MainGame.playSound("accept.wav");
					imgNumber=0;
					ImageIcon ii = new ImageIcon(this.getClass().getResource(imgPaths[imgNumber]));
					this.image = ii.getImage();
					mainGame.startGame();
				}
				else if (imgNumber==4){
					MainGame.playSound("accept.wav");
					imgNumber=0;
					ImageIcon ii = new ImageIcon(this.getClass().getResource(imgPaths[imgNumber]));
					this.image = ii.getImage();
					mainGame.startCustomGame();
				}
			}

		}
		previousKeyPressed= keyPressed;	
	}
	
	public void Draw(GamePanel gamePanel,Graphics2D g2d){
		g2d.drawImage(image,x,y,gamePanel);
	}

	public Rectangle getBounds() {
        return new Rectangle(x, y, this.image.getWidth(null), this.image.getHeight(null));
    }	
}
