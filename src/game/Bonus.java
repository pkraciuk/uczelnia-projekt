package game;


import game.contract.IBall;
import game.contract.IBonus;
import game.contract.IBonusInfo;
import game.contract.IBouncer;
import game.contract.ISprite;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import javax.swing.ImageIcon;

public class Bonus implements ISprite, IBonus{
	
	private IBouncer bouncer;
	private IBall ball;
	private IBonusInfo gamePanel;
	private int ySpeed;
	private int x;
	private int y;
	private Image image;
	private int type;
	
	private boolean visible;	
	public boolean getVisible(){
		return this.visible;
	}

	public Bonus(Bouncer bouncer,IBonusInfo mainGame,IBall ball) {
		this.ball=ball;
		this.visible=false;
		this.gamePanel=mainGame;
		this.type = (int)(Math.random()*7);
		this.bouncer =bouncer;
		String imgPath;
		switch (this.type){
		case 0:
			imgPath="images/bonus-laser.png";
		case 1:
			imgPath="images/bonus-long.png";
			break;
		case 2:
			imgPath="images/bonus-slow.png";
			break;
		case 3:
			imgPath="images/bonus-fast.png";
			break;
		case 4:
			imgPath="images/bonus-short.png";
			break;
		case 5:
			imgPath="images/bonus-life.png";
			break;
		case 6:
			imgPath="images/bonus-laser.png";
			break;
		default:
			imgPath="bonus-laser.png";
			break;
		}
		ImageIcon ii = new ImageIcon(this.getClass().getResource(imgPath));
		this.image = ii.getImage();
		
	}
	
	public void Update(){
		if (this.visible)
		{
			this.y+=this.ySpeed;
			if (CheckCollision(bouncer))
			{
				this.visible = false;
				MainGame.playSound("bonus.wav");
				switch(this.type)
				{
				case 0:
					bouncer.setImage("images/bouncer-short.png");
					gamePanel.setBonusInfoText("Short");
				case 1:
					bouncer.setImage("images/bouncer-long.png");
					gamePanel.setBonusInfoText("Long");
					break;
				case 2:
					ball.setSpeed(2);
					gamePanel.setBonusInfoText("Slow");
					break;
				case 3:
					ball.setSpeed(4);
					gamePanel.setBonusInfoText("Fast");
					break;
				case 4:
					bouncer.setImage("images/bouncer-short.png");
					gamePanel.setBonusInfoText("Short");
					break;
				case 5:
					gamePanel.updateLives(+1);
					gamePanel.setBonusInfoText("Life");
					break;
				case 6:
					gamePanel.setBonusInfoText("Laser");
					bouncer.setLaserActive(true);
					break;
				default:
				break;
				}
			}
			if (this.y+this.image.getHeight(null)==750)
			{
				this.visible=false;
				this.image=null;
			}
		}
		
		
	}
	
	public void Draw(GamePanel gamePanel,Graphics2D g2d){
		g2d.drawImage(image,x,y,gamePanel);
	}
	
	public Rectangle getBounds() {
        return new Rectangle(x, y, this.image.getWidth(null), this.image.getHeight(null));
    }
	
	public void Start(int x, int y){
		this.x=x;
		this.y=y;
		this.ySpeed=2;
		this.visible = true;

	}

	private boolean CheckCollision(Object o){
		ISprite bs = (ISprite)o;
		Rectangle r1 = this.getBounds();
		Rectangle r2 = bs.getBounds();
		if (r1.intersects(r2))
			return true;
		return false;
	}

}
