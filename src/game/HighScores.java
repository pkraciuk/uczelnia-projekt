package game;

import game.contract.IActiveSprite;
import game.contract.ISpriteGame;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;

import javax.swing.ImageIcon;

public class HighScores implements IActiveSprite {
	
	private ISpriteGame mainGame;
	private int x;
	private int y;
	private Image image;
	private int previousKeyPressed;
	
	private boolean visible;
	public boolean GetVisible(){
		return this.visible;
	}
	
	public HighScores(ISpriteGame mainGame, int x, int y) {
		this.mainGame = mainGame;
		previousKeyPressed=KeyEvent.VK_SPACE;
		this.visible=true;
		this.x=x;
		this.y=y;
		ImageIcon ii = new ImageIcon(this.getClass().getResource("images/highScores.png"));
		this.image = ii.getImage();
	}
	
	public void Update(int keyPressed, int keyReleased) {
		if((keyPressed == KeyEvent.VK_SPACE )&& previousKeyPressed!=keyPressed)
		{
			this.mainGame.startMenu();
		}
		previousKeyPressed=keyPressed;
		
	}
	
	public void Draw(GamePanel gamePanel,Graphics2D g2d){
		g2d.drawImage(image,x,y,gamePanel);
	}
	
	public Rectangle getBounds() {
        return new Rectangle(x, y, this.image.getWidth(null), this.image.getHeight(null));
    }
	


}
