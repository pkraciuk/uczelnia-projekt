package game;

import game.contract.IMouseSprite;
import game.contract.ISpriteGame;

import java.awt.Graphics2D;
import java.awt.Image;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import javax.swing.ImageIcon;

public class LevelEditorBackground implements IMouseSprite{

	private int x;
	private int y;
	private Image image;
	private int posX = 0;
	private int posY = 0;
	private int rzad = 0;
	private int kolumna = 0;
	private String info="";
	private int[] blockType = new int[84];
	
	private ISpriteGame mainGame;
	public ISpriteGame getGamePanel() {
		return mainGame;
	}
	public void setGamePanel(ISpriteGame mainGame) {
		this.mainGame = mainGame;
	}

	private List<Block> blockList;
	public void setBlockList(List<Block> blockList) {
		this.blockList = blockList;
	}
	public List<Block> getBlockList() {
		return blockList;
	}	
	
	public int[] getBlockType() {
		return blockType;
	}
	public void setBlockType(int[] blockType) {
		this.blockType = blockType;
	}

	private boolean visible;
	public boolean GetVisible() {
		return this.visible;
	}

	public LevelEditorBackground() {
		this.visible = true;
		this.x = 0;
		this.y = 0;
		ImageIcon ii = new ImageIcon(this.getClass().getResource("images/editorBackground.png"));
		this.image = ii.getImage();
	}

	public void Update(int mousePosX, int mousePosY, boolean mouseClicked) {
		this.posX = mousePosX;
		this.posY = mousePosY;
		if (mouseClicked) {
			
			if (posY > 120 && posY < 170) {
				rzad = 0;
				kolumna = getFirstColumn();
			} else if (posY > 170 && posY < 220) {
				rzad = 1;
				kolumna = getSecondColumn();
			} else if (posY > 220 && posY < 270) {
				rzad = 2;
				kolumna = getFirstColumn();
			} else if (posY > 270 && posY < 320) {
				rzad = 3;
				kolumna = getSecondColumn();
			} else if (posY > 320 && posY < 370) {
				rzad = 4;
				kolumna = getFirstColumn();
			} else if (posY > 370 && posY < 420) {
				rzad = 5;
				kolumna = getSecondColumn();
			} else {
				rzad = -1;
				kolumna = -1;
			}
			if (rzad >= 0 && kolumna >= 0) {
				MainGame.playSound("click.wav");
				this.info="";
				blockList.get(rzad * 14 + kolumna).clickBlock();
				if (blockType[rzad * 14 + kolumna] == 6)
					blockType[rzad * 14 + kolumna] = 0;
				else
					blockType[rzad * 14 + kolumna]++;
			}
			if (posY> 585&& posY<645 && posX>535 && posX< 730) {
				MainGame.playSound("click1.wav");
				saveLevel();
			}
			if (posY > 675 && posY<735 && posX>535 && posX<730){
				MainGame.playSound("click1.wav");
				this.mainGame.startMenu();
			}
		}
	}
	
	public void Draw(GamePanel gamePanel, Graphics2D g2d) {
		g2d.drawImage(image, x, y, gamePanel);
		g2d.drawString(info,200,450);

	}
	
	private int getFirstColumn() {
		if (posX > 45 && posX < 95) {
			return 0;
		} else if (posX > 95 && posX < 145) {
			return 1;
		} else if (posX > 145 && posX < 195) {
			return 2;
		} else if (posX > 195 && posX < 245) {
			return 3;
		} else if (posX > 245 && posX < 295) {
			return 4;
		} else if (posX > 295 && posX < 345) {
			return 5;
		} else if (posX > 345 && posX < 395) {
			return 6;
		} else if (posX > 395 && posX < 445) {
			return 7;
		} else if (posX > 445 && posX < 495) {
			return 8;
		} else if (posX > 495 && posX < 545) {
			return 9;
		} else if (posX > 545 && posX < 595) {
			return 10;
		} else if (posX > 595 && posX < 645) {
			return 11;
		} else if (posX > 645 && posX < 695) {
			return 12;
		} else if (posX > 695 && posX < 745) {
			return 13;
		} else {
			return -1;
		}
	}

	private int getSecondColumn() {
		if (posX > 70 && posX < 115) {
			return 0;
		} else if (posX > 115 && posX < 170) {
			return 1;
		} else if (posX > 170 && posX < 215) {
			return 2;
		} else if (posX > 215 && posX < 270) {
			return 3;
		} else if (posX > 270 && posX < 315) {
			return 4;
		} else if (posX > 315 && posX < 370) {
			return 5;
		} else if (posX > 370 && posX < 415) {
			return 6;
		} else if (posX > 415 && posX < 470) {
			return 7;
		} else if (posX > 470 && posX < 515) {
			return 8;
		} else if (posX > 515 && posX < 570) {
			return 9;
		} else if (posX > 570 && posX < 615) {
			return 10;
		} else if (posX > 615 && posX < 670) {
			return 11;
		} else if (posX > 670 && posX < 715) {
			return 12;
		} else if (posX > 715 && posX < 770) {
			return 13;
		} else {
			return 0;
		}
	}

	private void saveLevel() {
		boolean empty = true;
		for (int i : blockType) {
			if (i > 0) {
				empty = false;
				break;
			}
		}
		if (!empty) {
			String levelText = "<?sml version=\"1.0\" encoding=\"UTF-8\"?>\n<!DOCTYPE beans PUBLIC \"-//SPRING//DTD BEAN 2.0//EN\" \"http://www.springframework.org/dtd/spring-beans-2.0.dtd\">\n<beans>\n<bean id=\"custom-level\" class=\"game.LevelStructure\">\n<property name=\"blockType\">\n<list>\n";
			for (int i = 0; i < 84; i++) {
				levelText += "<value>" + blockType[i] + "</value>\n";
			}
			levelText += "</list>\n</property>\n</bean>\n</beans>";
			BufferedWriter pw;
			try {
				pw = new BufferedWriter(new FileWriter("custom-level.xml"));
				pw.write(levelText);
				pw.flush();
				pw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			this.mainGame.startMenu();
		}
		else{
			this.info="You can not save empty level";
		}

	}

}
