package game;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JPanel;
import javax.swing.Timer;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class GamePanel extends JPanel implements ActionListener{

	private static final long serialVersionUID = 1L;
	private Timer timer;
	private MainGame mainGame;
	private int keyPressed;
	private int keyReleased;
	private int mousePosX;
	private int mousePosY;
	private boolean mouseClicked;
	private boolean mouseReleased;
	private ApplicationContext context;
	
	public GamePanel(){
		context = new ClassPathXmlApplicationContext("spring.xml");
		mouseReleased=true;
		setFocusable(true);
		setBackground(Color.BLACK);
		setDoubleBuffered(true);
		this.addKeyListener(new TAdapter());
		this.addMouseListener(new MouseListener());
		mainGame=(MainGame)context.getBean("mainGame");
		timer = new Timer(5,this);
		timer.start();
	}
	
	class MouseListener extends MouseAdapter {
        public void mousePressed(MouseEvent e) {
        	mousePosX=e.getX();
        	mousePosY=e.getY();
        	if(e.getButton()==MouseEvent.BUTTON1){
        		mouseClicked=true;
        	}
        	else{
        		mouseClicked=false;
        	}
        }
    }
	
	public void actionPerformed(ActionEvent e) {
		if (mouseReleased==false)
		{
			mouseClicked=false;
			mouseReleased=true;
		}
	    mainGame.Update(this.keyPressed,this.keyReleased,mousePosX, mousePosY,mouseClicked);
	    keyReleased=0;
		repaint();	
		if (mouseClicked==true)
			mouseReleased=false;
	}
	
	public void paint(Graphics g){
		super.paint(g);
		Graphics2D g2d = (Graphics2D)g;
		mainGame.Draw(this,g2d);
		Toolkit.getDefaultToolkit().sync();
		g.dispose();
	}

	private class TAdapter extends KeyAdapter{
        public void keyPressed(KeyEvent e) {
            KPressed(e);
        }
        public void keyReleased(KeyEvent e){
        	KReleased(e);
        }
	}
	
	public void KPressed(KeyEvent e){
		this.keyPressed = e.getKeyCode();
		
	}
	
	public void KReleased(KeyEvent e){
		int k=e.getKeyCode();
		if (k==keyPressed)
			keyPressed=0;
		keyReleased = k;
	}

}
