package game.contract;

import game.Block;
import game.GamePanel;

import java.awt.Graphics2D;
import java.util.List;

public interface IMouseSprite {
	void Update(int mousePosX, int mousePosY, boolean mouseClicked);
	void Draw(GamePanel gamePanel, Graphics2D g2d);
	void setBlockType(int[] blockType);
	void setGamePanel(ISpriteGame mainGame);
	void setBlockList(List<Block> blockList);
}
