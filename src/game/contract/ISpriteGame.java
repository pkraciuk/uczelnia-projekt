package game.contract;

public interface ISpriteGame {
	void updateLives(int update);
	void updateScore(int update);
	void updateBlockNumber(int update);
	int GetScore();
	void startGame();
	void startMenu();
	void startHiScore();
	void startLevelEditor();
	void startCustomGame();
}
