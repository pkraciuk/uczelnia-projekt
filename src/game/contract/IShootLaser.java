package game.contract;

public interface IShootLaser {
	public void Shoot(int x, int y);
	public boolean getVisible();
}
