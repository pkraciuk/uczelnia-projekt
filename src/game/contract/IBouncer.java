package game.contract;

import java.awt.Rectangle;

public interface IBouncer {
	void setImage(String imgPath);
	Rectangle getBounds();
	void setLaserActive(boolean laserActive);
}
