package game.contract;

public interface IBonusInfo {
	void setBonusInfoText(String bonusInfoText);
	void updateLives(int update);
}
