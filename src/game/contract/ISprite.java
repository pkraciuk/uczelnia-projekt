package game.contract;

import game.GamePanel;

import java.awt.Graphics2D;
import java.awt.Rectangle;

public interface ISprite {
	void Update();
	void Draw(GamePanel gamePanel,Graphics2D g2d);
	boolean getVisible();
	Rectangle getBounds();
}
