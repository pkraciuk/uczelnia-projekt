package game.contract;

import java.awt.Rectangle;

public interface IBlock {
	Rectangle getBounds();
	void hitBlock();
	boolean getVisible();
	
}
