package game.contract;

import game.GamePanel;

import java.awt.Graphics2D;

public interface IActiveSprite {
	void Update(int keyPressed,int keyReleased);
	void Draw(GamePanel gamePanel,Graphics2D g2d);
}
