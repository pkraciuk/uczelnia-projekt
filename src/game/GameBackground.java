package game;

import game.contract.ISprite;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;

import javax.swing.ImageIcon;

public class GameBackground implements ISprite{
	protected int x;
	protected int y;
	protected Image image;
	
	protected boolean visible;
	public boolean getVisible() {
		return visible;
	}
	
	public GameBackground(int x, int y) {
		this.x=x;
		this.y=y;
		ImageIcon ii = new ImageIcon(this.getClass().getResource("images/panel.png"));
		this.image = ii.getImage();
		this.visible=true;
	}
	
	public void Update() {
	}

	public void Draw(GamePanel gamePanel, Graphics2D g2d) {
		g2d.drawImage(image,x,y,gamePanel);
	}

	public Rectangle getBounds() {
		return new Rectangle(x, y, this.image.getWidth(null), this.image.getHeight(null));
	}

}
