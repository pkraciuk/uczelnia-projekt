package game;

import game.contract.IShootLaser;
import game.contract.ISprite;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.util.List;

import javax.swing.ImageIcon;

public class LaserBeam implements ISprite,IShootLaser{
	
	private int ySpeed;
	private Image image;
	private int x;
	private int y;
	
	private List<Block> blockList;
	public void setBlockList(List<Block> blockList) {
		this.blockList = blockList;
	}
	
	private boolean visible;
	public boolean getVisible() {
		return this.visible;
	}
	
	public LaserBeam(List<Block> blockList){	
		this.visible = false;
		this.ySpeed=-5;
		ImageIcon ii = new ImageIcon(this.getClass().getResource("images/laserbeam.png"));
		this.image = ii.getImage();
		this.blockList=blockList;
		this.x=0;
		this.y=0;
	}
	
	public void Update() {
		if (this.visible)
		{
			this.y+=this.ySpeed;
			for (Block object:blockList)
			{
				if(CheckCollision(object)&& object.getVisible())
				{
					object.hitBlock();
					this.visible=false;
				}
			}
			if (this.y<=100)
			{
				this.visible=false;
			}
		}	
	}
	
	public void Draw(GamePanel gamePanel, Graphics2D g2d) {
		g2d.drawImage(image,x,y,gamePanel);
		
	}

	public Rectangle getBounds() {
		return new Rectangle(x, y, this.image.getWidth(null), this.image.getHeight(null));
	}
	
	private boolean CheckCollision(Object o){
	ISprite bs = (ISprite)o;
	Rectangle r1 = this.getBounds();
	Rectangle r2 = bs.getBounds();
	if (r1.intersects(r2))
		return true;
	return false;
}
	
	public void Shoot(int x, int y) {
		this.x=x;
		this.y=y;
		this.visible = true;
		
	}

}
